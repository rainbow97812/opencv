///////////////////////////////////////////////////
//미분을 이용하여 이미지의 윤곽선을 샤프하게 만듦.
//샤프하게 만드는 함수 3가지를 사용하는 코드
///////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	//실험, 대조군
	int val[] = { 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1,
	1, 1, 9, 9, 1, 1,
	1, 1, 9, 9, 1, 1,
	1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1 };
	Mat A(6, 6, CV_32F, val);

	//커널(3 X 3)의 가로와 세로값을 미분을 통하여 아웃풋하는 함수 
	Mat devX, devY;
	getDerivKernels(devX, devY, 0, 1, 3);

	//결과 가로와 세로값을 리턴합
	cout << devX << endl;
	cout << devY << endl;

	//가로와 세로로 만들어진 커널을 통해 필터링을 시도
	Mat res;
	sepFilter2D(A, res, -1, devX, devY, Point(-1, -1));
	cout << res << endl;

	//(3 X 3)커널을 내부적으로 만들어 이미지에 곱을 하고
	//이웃관에 결과를 (X1 - X2)로 거리를 재어 색의 차이가 큰 부분을
	//(윤곽선 contour)를 추출 가능 하게 함

	//dx dy는 dx가 1일 땐 세로로 이미지 차이(거리)를 계산하고
	//dy가 1일 땐 가로로 이미지 차이(거리)를 계산.
	Sobel(A, res, -1, 0, 2, 3);
	cout << res << endl;

	Scharr(A, res, -1, 0, 1, 1.0);
	cout << res << endl;
	return 0;
}