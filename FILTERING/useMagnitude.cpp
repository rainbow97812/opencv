#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	Mat img = imread("D:/img/fruit.jpg", IMREAD_GRAYSCALE);
	Mat resX, resY, res;

	Sobel(img, resX, CV_32F, 1, 0, 3);
	Sobel(img, resY, CV_32F, 0, 1, 3);

	magnitude(resX, resY, res);
	normalize(res, res, 0, 255, NORM_MINMAX, CV_8U);
	
	namedWindow("title", WINDOW_NORMAL);
	imshow("title", res);
	waitKey();

	resX.release();
	resY.release();
	res.release();
	return 0;
}