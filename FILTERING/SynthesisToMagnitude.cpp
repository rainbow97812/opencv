////////////////////////////////////////////////////////
//magnitude를 이용하여 X와 Y의 결과를 결합하여 도출
////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	Mat img = imread("D:/img/fruit.jpg");
	Mat resX, resY, res;
	vector<Mat> imgList, imgList2;

	Mat img2;
	flip(img, img2, 0);
	
	normalize(img, img, 0.0, 1.0, NORM_MINMAX, CV_32F);
	normalize(img2, img2, 0.0, 1.0, NORM_MINMAX, CV_32F);

	split(img, imgList);
	split(img2, imgList2);

	magnitude(imgList[0], imgList2[0], imgList[0]);
	magnitude(imgList[1], imgList2[1], imgList[1]);
	magnitude(imgList[2], imgList2[2], imgList[2]);

	merge(imgList, img);

	imshow("title", img);
	waitKey();

	return 0;
}