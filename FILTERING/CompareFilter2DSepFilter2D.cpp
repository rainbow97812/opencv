//////////////////////////////////////////////////////////////
//filter2D와 sepFilter2D의 성능 차이 비교
//(900 X 1200)크기의 이미지 실험군, 대조군
/////////////////////////////////////////////////////////////
#include <iostream>
#include <time.h>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;

int main() {
	double start, end;
	Mat img = imread("D:/img/fruit.jpg");
	Mat kernel(7, 7, CV_32F, 1. / 27.);
	Mat res;
	
	start = clock();
	filter2D(img, res, -1, kernel, Point(-1, -1));
	end = clock();

	cout << "time1: " << (end - start) / CLOCKS_PER_SEC << endl;

	//imshow("title", res);
	//waitKey();

	Mat kernelX(1, 7, CV_32F, 1. / 27.);
	Mat kernelY(7, 1, CV_32F, 1. / 27.);
	Mat res2;

	start = clock();
	sepFilter2D(img, res2, -1, kernelX, kernelY, Point(-1, -1));
	end = clock();

	cout << "time2: " << (end - start) / CLOCKS_PER_SEC << endl;

	//imshow("title2", res);
	//waitKey();

	return 0;
	//결과 time1: 
}