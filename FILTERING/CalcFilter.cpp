/////////////////////////////////////////////////////////
//filter2D는 이웃을 고려하여 이웃간의 상관관계를 상승 시킴
//커널과 주변 행을 곱셈하여 더해 값을 평균과 비슷하게 전환
//
//sepFilter2D는 kernelX로 계산 후 kernelY를 상관관계를 통해
//유사 계산을 하기에 더욱 빠른 속도를 낼 수 있음
/////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	//커널 사이즈(3 X 3)
	float kernel_val[] = { 1. / 9., 1. / 9., 1. / 9.,
		1. / 9., 1. / 9., 1. / 9.,
		1. / 9., 1. / 9., 1. / 9. };
	//소스 (5 X 5)
	float src_val[] = { 1, 2, 3, 4, 5,
	4, 3, 2, 1, 0,
	9, 8, 7, 1, 2,
	3, 5, 7, 9, 5,
	7, 4, 2, 5, 6 };
	Mat src(5, 5, CV_32F, src_val), dst;
	Mat kernel(3, 3, CV_32F, kernel_val);

	//ddepth가 -1이면 src.depth == dst.depth
	//Point(-1, -1)은 kernel의 정 가운대를 가리킴
	filter2D(src, dst, -1, kernel, Point(-1, -1));
	cout << dst << endl;

	Mat dst2;
	filter2D(dst, dst2, -1, kernel, Point(-1, -1));
	cout << dst2 << endl;

	//ddepth가 -1이면 src.depth == dst.depth
	//Point(2, 2)은 kernel의 (2, 2)를 중점으로 선정
	Mat dst3;
	filter2D(dst2, dst3, -1, kernel, Point(2, 2));
	cout << dst3 << endl;

	//kernelX(1 X 3) kernelY(3 X 1)로 유사 filter가 가능
	//회선(상관관계)를 이용하여 빠른 유사계산이 가능
	float kernelX_val[] = { 1. / 3., 1. / 3., 1. / 3. };
	float kernelY_val[] = { 1. / 3., 1. / 3., 1. / 3. };
	Mat kernelX(1, 3, CV_32F);
	Mat kernelY(3, 1, CV_32F);

	//src와 kernelX로 filter계산을 하고 나머지(2 X N)부분은 
	//kernelY와 filter계산 결과를 회선을 구하여 유사계산
	sepFilter2D(src, dst, -1, kernelX, kernelY, Point(-1, -1));
	cout << dst << endl;

	return 0;
}
