/////////////////////////////////////////////////////
//blur, medianBlur 사용
//blur는 filter와 비슷하게 흐리게
//medianBlur는 외부 경계를 유지하며 수채화풍의 그림처럼
//그림을 뭉그트리는 효과를 줌
/////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	Mat img = imread("D:/img/fruit.jpg");
	Mat res, res2;
	vector<Mat> hsvRes1, hsvRes2;
	if (img.empty()) {
		cout << "load error" << endl;
		return -1;
	}

	cvtColor(img, img, COLOR_BGR2HSV);
	split(img, hsvRes1);

	medianBlur(img, res, 21);

	cvtColor(res, res, COLOR_BGR2HSV);
	split(res, hsvRes2);

	Mat resImg;

	resImg.push_back(hsvRes1[0]);
	resImg.push_back(hsvRes2[0]);

	namedWindow("title", WINDOW_NORMAL);
	imshow("title", resImg);
	waitKey();

	//blur(img, res2, Size(21, 21));

	//namedWindow("title1", WINDOW_NORMAL);
	//imshow("title1", res2);
	//waitKey();

	imwrite("D:/img/differenceRawImgBlurImg.jpg", resImg);

	return 0;
}