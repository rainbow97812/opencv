//////////////////////////////////////////////////////////////////////////
//explain code:
//이퀄라이징 된 이미지와 이퀄라이징 전의 이미지의 히스토그램을 비교.
//이퀄라이징을 통하여 더욱 명확한 색상을 표현
///////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;
int main() {
	//load Image
	Mat img = imread("D:/img/desert.png");
	//hist is destination of calcHist result
	//graph is show histogram graph of non_equalization_image
	//res_graph is show histogram graph of equalization_image
	Mat hist, graph(256, 256, CV_8U), res_graph(256, 256, CV_8U);
	//To save split img 
	vector<Mat> matList;
	//0 Blue 1 Green 2 Red
	//{0, 1}, {0, 2}, {1. 2}
	//all channels is different each compare channel num
	int channels[] = { 0, 1 };
	//Output(hist) Dimention shape
	int histSize[] = { 64 };
	//Draw a Barplot in range [0, 255]  
	float range[] = { 0, 256 };
	//save a each range
	//if want to draw 2Dimetion Graph need a Two Range (Row range, Col Range)
	const float* ranges[] = { range };

	//one_Bar_Columns_Size
	int barColSize = range[1] / histSize[0];

	//calculate Histogram
	//calculate Histogram and get result img to hist param
	calcHist(&img, 1, channels, Mat(), hist, 1, histSize, ranges, true);
	//To make normalize value to [0, 255], and change type 8bit UCHAR (-256 ~ 255)
	normalize(hist, hist, 0, 255, NORM_MINMAX, CV_8U);

	//MatIterator
	MatConstIterator_<uchar> it = hist.begin<uchar>();
	for (int n = 0; it != hist.end<uchar>(); it++, n++) {
		//make one_bar to draw Histogram result Sequencely
		rectangle(graph, Rect(Point2i(n*barColSize, 0), Size2i(barColSize, (int)*it)), Scalar(0), 2);
	}

	//filp img top-bottom to make show easily
	flip(graph, graph, 0);
	//save a graph
	imwrite("D:/img/graph.png", graph);

	//split img to each channels
	//equalizeHist doesn't calculate multi-channels
	split(img, matList);

	//To calculate equalizeHist each channels
	equalizeHist(matList[0], matList[0]);
	equalizeHist(matList[1], matList[1]);
	equalizeHist(matList[2], matList[2]);

	//merge each channels
	merge(matList, img);

	//calculate Histogram
	//calculate Histogram and get result img to hist param
	calcHist(&img, 1, channels, Mat(), hist, 1, histSize, ranges, true);
	//To make normalize value to [0, 255], and change type 8bit UCHAR (-256 ~ 255)
	normalize(hist, hist, 0, 255, NORM_MINMAX, CV_8U);

	//MatIterator
	MatConstIterator_<uchar> it2 = hist.begin<uchar>();
	for (int n = 0; it2 != hist.end<uchar>(); it2++, n++) {
		//make one_bar to draw Histogram result Sequencely
		rectangle(res_graph, Rect(Point2i(n * barColSize, 0), Size2i(barColSize, (int)*it2)), Scalar(0), 2);
	}

	//filp img top-bottom to make show comfortablely
	flip(res_graph, res_graph, 0);
	//save a graph
	imwrite("D:/img/res_graph.png", res_graph);

	//show histogram make to bundle
	Mat tot_graph;
	tot_graph.push_back(graph);
	tot_graph.push_back(res_graph);

	imshow("title", tot_graph);
	waitKey();

	//release(free) Mat
	graph.release();
	res_graph.release();
	tot_graph.release();
	return 0;
}