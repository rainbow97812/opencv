///////////////////////////////////////////////////////////////////////////
//explain:
//이퀄라이징 된 이미지와 raw이미지를 비교 -VER2
///////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;
int main() {
	//load Image
	Mat img = imread("D:/img/desert.png");
	//hist is destination of calcHist result
	//graph is show histogram graph of non_equalization_image
	//res_graph is show histogram graph of equalization_image
	Mat hist, eq_hist, graph(256, 256, CV_8U), res_graph(256, 256, CV_8U);
	//To save split img 
	vector<Mat> matList;
	//0 Blue 1 Green 2 Red
	//{0, 1}, {0, 2}, {1. 2}
	//all channels is different each compare channel num
	int channels[] = { 0, 1 };
	//Output(hist) Dimention shape
	int histSize[] = { 64 };
	//Draw a Barplot in range [0, 255]  
	float range[] = { 0, 256 };
	//save a each range
	//if want to draw 2Dimetion Graph need a Two Range (Row range, Col Range)
	const float* ranges[] = { range };

	//one_Bar_Columns_Size
	int barColSize = range[1] / histSize[0];

	//calculate Histogram
	//calculate Histogram and get result img to hist param
	calcHist(&img, 1, channels, Mat(), hist, 1, histSize, ranges, true);

	//split img to each channels
	//equalizeHist doesn't calculate multi-channels
	split(img, matList);

	//To calculate equalizeHist each channels
	equalizeHist(matList[0], matList[0]);
	equalizeHist(matList[1], matList[1]);
	equalizeHist(matList[2], matList[2]);

	//merge each channels
	merge(matList, img);

	//calculate Histogram
	//calculate Histogram and get result img to hist param
	calcHist(&img, 1, channels, Mat(), eq_hist, 1, histSize, ranges, true);

	//return CORRELATION of hist and eq_hist
	cout << "HIST CORRELATION VALUE: " << compareHist(hist, eq_hist, HISTCMP_CORREL) << endl;

	return 0;

	//result: -0.66
}