/////////////////////////////////////////////////////////////
//explain:
//정규분포를 HIST를 이용하여 도표를 그리고 EMD를 표시 
//합니다.
/////////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;

//바 그래프를 그려줍니다 
void barplot(InputArray valMat, Mat& ground, int barColSize);

int main() {
	//노말 표준정규분포를 그려 줄 매트릭스
	Mat srcImage1(512, 512, CV_8U);
	Mat srcImage2(512, 512, CV_8U);

	//RNG를 이용하여 정규분포를 그림
	RNG rng1 = theRNG();
	Scalar mean1 = Scalar(128);
	Scalar stddev1 = Scalar(20);
	rng1.fill(srcImage1, RNG::NORMAL, mean1, stddev1);

	RNG rng2 = theRNG();
	Scalar mean2 = Scalar(64);
	Scalar stddev2 = Scalar(10);
	rng2.fill(srcImage2, RNG::NORMAL, mean2, stddev2);

	//hist를 각각 그림
	Mat hist1, hist2;
	int channels[] = { 0 };
	int histSize[] = { 128 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };

	calcHist(&srcImage1, 1, channels, Mat(), hist1, 1, histSize, ranges, true);
	calcHist(&srcImage2, 1, channels, Mat(), hist2, 1, histSize, ranges, true);

	//EDM계산을 위해 자료형을 바꿈
	normalize(hist1, hist1, 1, 0, NORM_MINMAX, CV_32F);
	normalize(hist2, hist2, 1, 0, NORM_MINMAX, CV_32F);

	//시그니쳐 매트릭스를 생성함
	Mat sig1(hist1.rows, 2, CV_32F);
	Mat sig2(hist2.rows, 2, CV_32F);
	
	//0인덱스에는 WEIGHT를 1인덱스에는 DISTANCE를 정의
	for (int i = 0; i < hist1.rows; i++) {
		sig1.at<float>(i, 0) = hist1.at<float>(i);
		sig1.at<float>(i, 1) = i + 1;
	}

	for (int i = 0; i < hist2.rows; i++) {
		sig2.at<float>(i, 0) = hist2.at<float>(i);
		sig2.at<float>(i, 1) = i + 1;
	}

	//EMD계산으로 최소의 거리를 계산 (FD / F)
	cout << EMD(sig1, sig2, DIST_L2) << endl;

	//바플롯을 그려서 img, img2에 저장
	Mat img, img2;
	barplot(hist1, img, 5);
	barplot(hist2, img2, 5);

	//imgList로 결과를 concat해서 도출
	Mat imgList;
	imgList.push_back(img);
	imgList.push_back(img2);

	imshow("title", imgList);
	waitKey();

	//free
	srcImage1.release();
	srcImage2.release();
	hist1.release();
	hist2.release();
	sig1.release();
	sig2.release();
	img.release();
	img2.release();
	imgList.release();

	return 0;
}

//바 그래프를 ground에 그림 
void barplot(InputArray valMat, Mat& ground, int barColSize) {
	int barNum = valMat.rows();
	ground  = Mat(256, barNum*barColSize, CV_8U);
	Mat val = valMat.getMat();
	
	normalize(val, val, 0, 255, NORM_MINMAX, CV_8U);

	for (int i = 0; i < barNum; i++) {
		rectangle(ground, Rect(Point2i(i * barColSize, 0), Size2i(barColSize, val.at<uchar>(i))), Scalar(0), 2);
	}

	flip(ground, ground, 0);
}