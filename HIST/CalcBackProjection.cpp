/////////////////////////////////////////////////////////
//이미지를 HSV로 바꾸어 H(hue)를 이용하여 원하는 물체의 
//border를 찾는 것이 가능.
//찾고 싶은 색상의 부분을 잘라 hist를 구하여 backproject
//시키면 근접 hue의 픽셀만 표시가 됨.
/////////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	//
	Mat fruit = imread("D:/img/fruit.jpg");
	if (fruit.empty()) {
		cout << "EMPTY!!" << endl;
		return -1;
	}

	//H hue(색상) S saturation(채도) V value(명도)
	//0~360회전 0(=360)은 RED값 Green Blue Red로 회전
	//색상에 따라 값이 달라짐을 이용하여 구별
	cvtColor(fruit, fruit, COLOR_BGR2HSV);
	vector<Mat> imgList;

	//HSV를 분리하여 H(색상)만 저장을 함
	split(fruit, imgList);
	Mat HMat = imgList[0];

	//찾고 싶은 색상의 위치를 사각형으로 표시
	Rect getPoint = Rect(Point2i(100, 600), Point2i(130, 630));
	rectangle(fruit, getPoint, Scalar(360, 100, 30), 4);
	rectangle(HMat, getPoint, Scalar(360, 100, 30), 4);
	
	//지정된 사각형 만큼 색상mat을 잘라서 저장
	Mat pointMat = HMat(getPoint);
	//부분 Mat을 histogram계산을 하여 색상평균 빈도를 구함
	Mat hist;
	int channels[] = { 0 };
	int histSize[] = { 256 };
	float range[] = { 0, 256 };
	const float* ranges[] = { range };
	calcHist(&pointMat, 1, channels, Mat(), hist, 1, histSize, ranges, true);

	//역투영을 실시하여 backProj에 결과를 그림
	Mat backProj;
	calcBackProject(&HMat, 1, channels, hist, backProj, ranges);

	//결과를 출력
	namedWindow("backProjection", WINDOW_NORMAL);
	namedWindow("HueMatrix", WINDOW_NORMAL);

	imshow("backProjection", backProj);
	imshow("HueMatrix", HMat);
	waitKey();
	return 0;
}