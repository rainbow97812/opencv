////////////////////////////////////////////////////
//이미지에서 저주파의 주파수만 필터링하는 계산. 
//2가지의 방법으로 고주파는 걸러버리는 함수.
//threshould처럼 기준을 중심으로 저, 고주파를 선정
//
////////////////////////////////////////////////////
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

//shift방식으로 x, y 기준을 변경
void shiftTranspose(Mat& src, Mat& dst);
//유클리디안 방식으로 거리를 내어 D0과 대조
void idealLowpassFilter(Mat& src, Mat& dst, float D0);
//유클리디안 거리의 역수(0 ~ 1) 0고주파 1저주파 
void butterworthFilter(Mat& src, Mat& dst, float D0, int n);

int main() {
	Mat img = imread("D:/img/fruit.jpg", IMREAD_GRAYSCALE);

	img.convertTo(img, CV_32F);
	//중점으로 x, y를 이동
	shiftTranspose(img, img);
	Mat dst;
	vector<Mat> resList;

	//푸리에 변환
	dft(img, dst, DFT_COMPLEX_OUTPUT);
	//이상 저주파필터링으로 결과는 냄
	idealLowpassFilter(dst, dst, 3);
	//butterworthFilter로 결과를 냄
	//butterworthFilter(dst, dst, 300, 10);

	//inverse푸리에 변환으로 다시 이미지 값으로 변경
	idft(dst, img, DFT_SCALE);

	split(img, resList);
	//R과 I을 합성
	magnitude(resList[0], resList[1], img);

	normalize(img, img, 0, 255, NORM_MINMAX, CV_8U);
	//이미지 출력
	namedWindow("ti", WINDOW_NORMAL);
	imshow("ti", img);
	waitKey();

	return 0;
}

//i와 j의 나머지가 1이면 -를 곱함
void shiftTranspose(Mat& src, Mat& dst) {
	Mat d;
	d = src;

	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			if ((i + j) % 2 == 1)
				d.at<float>(i, j) *= -1;
		}
	}
	dst = d;
}

void idealLowpassFilter(Mat& src, Mat& dst, float D0) {
	float h, D;
	//중심 u = M/2 v = N/2
	float centerU = src.rows / 2;
	float centerV = src.cols / 2;
	vector<Mat> d;
	split(src, d);

	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			//유클리디안으로 중점과의 거리를 계산
			D = sqrt((i - centerU) * (i - centerU) + (j - centerV) * (j - centerV));
			//threshould와 비교를 하여 필터링
			if (D > D0) {
				d[0].at<float>(i, j) = 0.0;
				d[1].at<float>(i, j) = 0.0;
			}
		}
	}
	merge(d, dst);
}

void butterworthFilter(Mat& src, Mat& dst, float D0, int n) {
	float D;
	float H;
	int centerY = src.rows;
	int centerX = src.cols;
	vector<Mat> d;
	split(src, d);

	for (int i = 0; i < src.rows; i++) {
		for (int j = 0; j < src.cols; j++) {
			//유클리디안으로 거리를 계산
			D = sqrt((i - centerY)*(i - centerY) + (j - centerX)*(j - centerX));
			//유클리디안의 역수를 취함 (0 ~ 1)
			H = 1 / (1 + pow((D / D0), 2 * n));
			if (H < 0.5) {
				d[0].at<float>(i, j) = 0.0;
				d[1].at<float>(i, j) = 0.0;
			}
		}
	}

	merge(d, dst);
}