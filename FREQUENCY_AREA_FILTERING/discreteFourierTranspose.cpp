#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

int main() {
	float dataA[] = { 1, 2, 4, 5, 2, 1,
	3, 6, 6, 9, 0, 3,
	1, 8, -3, -7, 2, 5,
	2, 9, -8, -9, 9, 1,
	3, 9, 8, 8, 7, 2,
	4, 9, 9, 9, 9, 3 };

	Mat src(6, 6, CV_32F, dataA);
	Mat dst;
	vector<Mat> RI;

	dft(src, dst, DFT_COMPLEX_OUTPUT);

	cout << "channel: " << dst.channels() << endl;
	cout << dst << endl;
	cout << endl;

	split(dst, RI);

	cout << endl;
	cout << RI[0] << endl;
	cout << endl;
	cout << RI[1] << endl;

	idft(dst, src, DFT_COMPLEX_INPUT | DFT_SCALE);
	cout << "channel2: " << src.channels() << endl;
	cout << src << endl;

	split(src, RI);

	cout << endl;
	cout << RI[0] << endl;
	cout << endl;
	cout << RI[1] << endl;
	return 0;
}