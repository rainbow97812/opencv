# openCV

Practice openCV 

INDEX>
 HIST>
  1. 히스토그램을 그리는 법을 학습 (calcHist방법 EDM방법)
   
    1-1. calcHist 매트릭스의 각 채널의 값 빈도를 바그래프로 표현
    1-2. EDM 가중치와 거리를 구현하여 가장 적은 일을 구현 했을 때의 값을 출력

  2. 히스토그램을 활용하여 역투영 등을 실습
  
    - Contoure를 찾기에 용의